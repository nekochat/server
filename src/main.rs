use std::{collections::{VecDeque, HashMap}, mem, sync::Arc, net::SocketAddr, fs::File};

use chrono::Utc;
use futures_util::{StreamExt, TryStreamExt};
use mongodb::bson::{self, doc};
use rand::Rng;
use serde::{Deserialize, Serialize};
use tokio::{net::TcpListener, io::{AsyncReadExt, AsyncWriteExt}, sync::{mpsc, RwLock}};
use uuid::Uuid;
use nekochat::api::{self, request::{Request, RequestKind}, response::{Response, Body, Status, Error, self}, event::Event};

use db::Database;
use model::{User, UserId, SessionToken, Session, SessionId, Room, RoomId, Member, Channel, ChannelId, Message, MessageId};
use convert_uuid::UuidExt;

mod db;
mod model;
mod convert_uuid;

struct Parser {
    buffer: Vec<u8>,
    packet_queue: VecDeque<Request>,
}

impl Parser {
    fn parse(&mut self, bytes: &[u8]) {
        for byte in bytes {
            match byte {
                0x0a => {
                    self.packet_queue.push_back(serde_json::from_str(&String::from_utf8(mem::replace(&mut self.buffer, Vec::new())).unwrap()).unwrap())
                },
                _ => self.buffer.push(*byte),
            }
        }
    }

    fn get_packet(&mut self) -> Option<Request> {
        self.packet_queue.pop_front()
    }
}

#[derive(Debug, Serialize)]
#[serde(untagged)]
enum Packet {
    Response(Response),
    Event(Event),
}

struct Connection {
    send_queue: mpsc::Sender<Packet>,
    user_id: Option<UserId>,
}

#[derive(Deserialize)]
struct Config {
    address: String,
    #[serde(rename = "mongodbUri")]
    mongodb_uri: String,
    database: String,
}

#[tokio::main]
async fn main() {
    let config: Config = serde_json::from_reader(
        File::open("config.json").unwrap()
    ).unwrap();

    let socket = TcpListener::bind(config.address).await.unwrap();
    let connections = Arc::new(RwLock::new(HashMap::new()));

    let mongo = mongodb::Client::with_uri_str(&config.mongodb_uri).await.unwrap();
    let db = Database::new(mongo.database(&config.database));

    while let Ok((stream, addr)) = socket.accept().await {
        let (mut stream_read, mut stream_write) = stream.into_split();
        let (send_queue_send, mut send_queue_receive) = mpsc::channel(8);

        let connection = Arc::new(RwLock::new(Connection {
            send_queue: send_queue_send.clone(),
            user_id: None,
        }));
        connections.write().await.insert(addr, connection.clone());

        let connections = connections.clone();
        let db = db.clone();

        tokio::spawn(async move {
            let mut parser = Parser {
                buffer: Vec::new(),
                packet_queue: VecDeque::new(),
            };

            loop {
                let mut bytes = [0; 128];
                let byte_count = stream_read.read(&mut bytes).await.unwrap();

                if byte_count == 0 {
                    break;
                }

                parser.parse(&bytes[0..byte_count]);

                while let Some(request) = parser.get_packet() {
                    let Request { id, kind } = request;

                    send_queue_send.send(Packet::Response(Response { id, body: handle_request(&db, &connection, &connections, kind).await })).await.unwrap();
                }
            }

            connections.write().await.remove(&addr);
        });

        tokio::spawn(async move {
            while let Some(packet) = send_queue_receive.recv().await {
                stream_write.write_all(&[serde_json::to_vec(&packet).unwrap().as_slice(), &[0x0a]].concat()).await.unwrap();
            }
        });
    }
}

async fn handle_request(db: &Database, connection: &RwLock<Connection>, connections: &RwLock<HashMap<SocketAddr, Arc<RwLock<Connection>>>>, kind: RequestKind) -> Status {
    let require_authentication = || async {
        connection.read().await.user_id.ok_or(Error::Unauthenticated)
    };

    Ok(match kind {
        RequestKind::UserCreate { name, password } => {
            if db.get_user_by_name(&name).await.is_some() {
                return Err(Error::NameTaken);
            }

            let user = User {
                id: UserId(Uuid::new_v4().into_bson()),
                created_time: Utc::now(),
                name,
                password: bcrypt::hash(password, bcrypt::DEFAULT_COST).unwrap(),
            };
            db.users.insert_one(user, None).await.unwrap();

            Body::UserCreate(response::UserCreate {})
        },
        RequestKind::UserGet { user_id } => {
            let user = db.get_user(&UserId::from_api(user_id)).await.ok_or(Error::NotFound)?;

            Body::UserGet(response::UserGet { user: user.into_api() })
        },
        RequestKind::UserGetSelf => {
            let user_id = require_authentication().await?;

            let user = db.get_user(&user_id).await.unwrap();

            Body::UserGetSelf(response::UserGetSelf { user: user.into_api() })
        },
        RequestKind::UserGetSelfRoom => {
            let user_id = require_authentication().await?;

            let mut rooms = Vec::new();
            let mut members = db.members.find(doc! { "userId": user_id.0 }, None).await.unwrap();
            while members.advance().await.unwrap() {
                let member = members.deserialize_current().unwrap();
                let room = db.get_room(&member.room_id).await.unwrap();
                rooms.push(room.into_api());
            }

            Body::UserGetSelfRoom(response::UserGetSelfRoom { rooms })
        },
        RequestKind::SessionCreate { name, password } => {
            let user = db.get_user_by_name(&name).await.ok_or(Error::InvalidCredentials)?;
            if !bcrypt::verify(password, &user.password).unwrap() {
                return Err(Error::InvalidCredentials);
            }

            let mut bytes: [u8; 256] = [0; 256];
            rand::thread_rng().fill(&mut bytes);
            let token = SessionToken(bytes.to_vec());

            let session = Session {
                id: SessionId(Uuid::new_v4().into_bson()),
                user_id: user.id,
                created_time: Utc::now(),
                token: token.clone(),
            };
            db.sessions.insert_one(session, None).await.unwrap();

            Body::SessionCreate(response::SessionCreate { token: token.into_api() })
        },
        RequestKind::Authenticate { token } => {
            let bson_token = bson::Binary { subtype: bson::spec::BinarySubtype::Generic, bytes: token.0 };
            let session = db.sessions.find_one(doc! { "token": bson_token }, None).await.unwrap().ok_or(Error::InvalidCredentials)?;
            connection.write().await.user_id = Some(session.user_id);

            Body::AuthLogin(response::AuthLogin {})
        },
        RequestKind::RoomCreate { name } => {
            let user_id = require_authentication().await?;

            let room = Room {
                id: RoomId(Uuid::new_v4().into_bson()),
                owner_id: user_id.clone(),
                created_time: Utc::now(),
                name,
            };
            db.rooms.insert_one(&room, None).await.unwrap();

            Body::RoomCreate(response::RoomCreate { room: room.into_api() })
        },
        RequestKind::RoomGet { room_id } => {
            let user_id = require_authentication().await?;
            let (room, _) = user_id.access_room(&db, &RoomId::from_api(room_id)).await?;

            Body::RoomGet(response::RoomGet { room: room.into_api() })
        },
        RequestKind::RoomJoin { room_id } => {
            let user_id = require_authentication().await?;

            let member = Member {
                user_id: user_id.clone(),
                room_id: RoomId::from_api(room_id),
                created_time: Utc::now(),
            };
            db.members.insert_one(&member, None).await.unwrap();

            Body::RoomJoin(response::RoomJoin {})
        },
        RequestKind::RoomChannelGet { room_id } => {
            require_authentication().await?;

            let channels: Vec<api::Channel> = db.channels.find(doc! { "roomId": RoomId::from_api(room_id).0 }, None).await.unwrap()
                .map(|m| m.and_then(|m| Ok(m.into_api())))
                .try_collect().await.unwrap();

            Body::RoomChannelGet(response::RoomChannelGet { channels })
        },
        RequestKind::RoomChannelCreate { room_id, name } => {
            let user_id = require_authentication().await?;
            let room_id = RoomId::from_api(room_id);
            let (room, _) = user_id.access_room(&db, &room_id).await?;
            if room.owner_id != user_id {
                return Err(Error::Unauthorized);
            }

            let channel = Channel {
                id: ChannelId(Uuid::new_v4().into_bson()),
                room_id,
                created_time: Utc::now(),
                name,
            };
            db.channels.insert_one(&channel, None).await.unwrap();

            let api_channel = channel.into_api();
            for (_, connection) in &*connections.read().await {
                let connection = connection.read().await;
                if let Some(user_id) = connection.user_id {
                    if user_id.access_room(&db, &room_id).await.is_ok() {
                        connection.send_queue.send(
                            Packet::Event(Event::ChannelCreated { channel: api_channel.clone() })
                        ).await.unwrap();
                    }
                }
            }

            Body::RoomChannelCreate(response::RoomChannelCreate { channel: api_channel })
        },
        RequestKind::ChannelGet { channel_id } => {
            let user_id = require_authentication().await?;
            let (_, _, channel) = user_id.access_channel(&db, &ChannelId::from_api(channel_id)).await?;

            Body::ChannelGet(response::ChannelGet { channel: channel.into_api() })
        },
        RequestKind::ChannelMessageGet { channel_id } => {
            require_authentication().await?;

            let messages: Vec<api::Message> = db.messages.find(doc! { "channelId": ChannelId::from_api(channel_id).0 }, None).await.unwrap()
                .map(|m| m.and_then(|m| Ok(m.into_api())))
                .try_collect().await.unwrap();

            Body::ChannelMessageGet(response::ChannelMessageGet { messages })
        },
        RequestKind::ChannelMessageCreate { channel_id, content } => {
            let user_id = require_authentication().await?;
            let channel_id = ChannelId::from_api(channel_id);
            let (_, _, channel) = user_id.access_channel(&db, &channel_id).await?;

            let message = Message {
                id: MessageId(Uuid::new_v4().into_bson()),
                user_id: user_id.clone(),
                channel_id,
                created_time: Utc::now(),
                content,
            };
            db.messages.insert_one(&message, None).await.unwrap();

            let api_message = message.into_api();
            for (_, connection) in &*connections.read().await {
                let connection = connection.read().await;
                if let Some(user_id) = connection.user_id {
                    if user_id.access_room(&db, &channel.room_id).await.is_ok() {
                        connection.send_queue.send(
                            Packet::Event(Event::MessageCreated { message: api_message.clone() })
                        ).await.unwrap();
                    }
                }
            }

            Body::ChannelMessageCreate(response::ChannelMessageCreate { message: api_message })
        },
        RequestKind::MessageGet { message_id } => {
            let user_id = require_authentication().await?;
            let (_, _, _, message) = user_id.access_message(&db, &MessageId::from_api(message_id)).await?;

            Body::MessageGet(response::MessageGet { message: message.into_api() })
        },
    })
}

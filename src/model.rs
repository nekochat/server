use chrono::{DateTime, Utc};
use mongodb::bson::Uuid;
use serde::{Serialize, Deserialize};
use nekochat::api::{self, response};

use crate::{convert_uuid::{BsonExt, UuidExt}, db::Database};

#[derive(Serialize, Deserialize)]
pub struct User {
    #[serde(rename = "_id")]
    pub id: UserId,
    #[serde(rename = "createdTime")]
    #[serde(with = "serde_timestamp")]
    pub created_time: DateTime<Utc>,
    pub name: String,
    pub password: String,
}

#[derive(Serialize, Deserialize)]
pub struct Session {
    #[serde(rename = "_id")]
    pub id: SessionId,
    #[serde(rename = "userId")]
    pub user_id: UserId,
    #[serde(rename = "createdTime")]
    #[serde(with = "serde_timestamp")]
    pub created_time: DateTime<Utc>,
    pub token: SessionToken,
}

#[derive(Serialize, Deserialize)]
pub struct Room {
    #[serde(rename = "_id")]
    pub id: RoomId,
    #[serde(rename = "ownerId")]
    pub owner_id: UserId,
    #[serde(rename = "createdTime")]
    #[serde(with = "serde_timestamp")]
    pub created_time: DateTime<Utc>,
    pub name: String,
}

#[derive(Serialize, Deserialize)]
pub struct Member {
    #[serde(rename = "userId")]
    pub user_id: UserId,
    #[serde(rename = "roomId")]
    pub room_id: RoomId,
    #[serde(rename = "createdTime")]
    #[serde(with = "serde_timestamp")]
    pub created_time: DateTime<Utc>,
}

#[derive(Serialize, Deserialize)]
pub struct Channel {
    #[serde(rename = "_id")]
    pub id: ChannelId,
    #[serde(rename = "roomId")]
    pub room_id: RoomId,
    #[serde(rename = "createdTime")]
    #[serde(with = "serde_timestamp")]
    pub created_time: DateTime<Utc>,
    pub name: String,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Message {
    #[serde(rename = "_id")]
    pub id: MessageId,
    #[serde(rename = "userId")]
    pub user_id: UserId,
    #[serde(rename = "channelId")]
    pub channel_id: ChannelId,
    #[serde(rename = "createdTime")]
    #[serde(with = "serde_timestamp")]
    pub created_time: DateTime<Utc>,
    pub content: String,
}

#[derive(PartialEq, Clone, Copy, Serialize, Deserialize)]
pub struct UserId(pub Uuid);

#[derive(PartialEq, Clone, Copy, Serialize, Deserialize)]
pub struct SessionId(pub Uuid);

#[derive(PartialEq, Clone, Copy, Serialize, Deserialize)]
pub struct RoomId(pub Uuid);

#[derive(PartialEq, Clone, Copy, Serialize, Deserialize)]
pub struct ChannelId(pub Uuid);

#[derive(PartialEq, Clone, Copy, Serialize, Deserialize)]
pub struct MessageId(pub Uuid);

#[derive(PartialEq, Clone, Serialize, Deserialize)]
pub struct SessionToken(
    #[serde(with = "serde_bytes")]
    pub Vec<u8>
);

impl User {
    pub fn into_api(self) -> api::User {
        api::User {
            id: self.id.into_api(),
            created_time: self.created_time,
            name: self.name,
        }
    }
}

impl Room {
    pub fn into_api(self) -> api::Room {
        api::Room {
            id: self.id.into_api(),
            owner_id: self.owner_id.into_api(),
            created_time: self.created_time,
            name: self.name,
        }
    }
}

impl Channel {
    pub fn into_api(self) -> api::Channel {
        api::Channel {
            id: self.id.into_api(),
            room_id: self.room_id.into_api(),
            created_time: self.created_time,
            name: self.name,
        }
    }
}

impl Message {
    pub fn into_api(self) -> api::Message {
        api::Message {
            id: self.id.into_api(),
            user_id: self.user_id.into_api(),
            channel_id: self.channel_id.into_api(),
            created_time: self.created_time,
            content: self.content,
        }
    }
}

impl UserId {
    pub async fn access_message(&self, db: &Database, message_id: &MessageId) -> Result<(Room, Member, Channel, Message), response::Error> {
        let message = db.get_message(message_id).await.ok_or(response::Error::NotFound)?;
        let (room, member, channel) = self.access_channel(db, &message.channel_id).await?;
        Ok((room, member, channel, message))
    }

    pub async fn access_channel(&self, db: &Database, channel_id: &ChannelId) -> Result<(Room, Member, Channel), response::Error> {
        let channel = db.get_channel(channel_id).await.ok_or(response::Error::NotFound)?;
        let (room, member) = self.access_room(db, &channel.room_id).await?;
        Ok((room, member, channel))
    }

    pub async fn access_room(&self, db: &Database, room_id: &RoomId) -> Result<(Room, Member), response::Error> {
        let room = db.get_room(room_id).await.ok_or(response::Error::NotFound)?;
        let member = db.get_member(self, room_id).await.ok_or(response::Error::Unauthorized)?;
        Ok((room, member))
    }

    pub fn into_api(self) -> api::UserId {
        api::UserId(self.0.into_uuid())
    }

    pub fn from_api(user_id: api::UserId) -> Self {
        Self(user_id.0.into_bson())
    }
}

impl RoomId {
    pub fn into_api(self) -> api::RoomId {
        api::RoomId(self.0.into_uuid())
    }

    pub fn from_api(room_id: api::RoomId) -> Self {
        Self(room_id.0.into_bson())
    }
}

impl ChannelId {
    pub fn into_api(self) -> api::ChannelId {
        api::ChannelId(self.0.into_uuid())
    }

    pub fn from_api(channel_id: api::ChannelId) -> Self {
        Self(channel_id.0.into_bson())
    }
}

impl MessageId {
    pub fn into_api(self) -> api::MessageId {
        api::MessageId(self.0.into_uuid())
    }

    pub fn from_api(message_id: api::MessageId) -> Self {
        Self(message_id.0.into_bson())
    }
}

impl SessionToken {
    pub fn into_api(self) -> api::SessionToken {
        api::SessionToken(self.0)
    }

    pub fn from_api(session_token: api::SessionToken) -> Self {
        Self(session_token.0)
    }
}

mod serde_timestamp {
    use chrono::{DateTime, Utc, TimeZone};
    use serde::{Deserializer, Serializer, Deserialize};

    pub fn serialize<S>(date: &DateTime<Utc>, serializer: S) -> Result<S::Ok, S::Error>
        where S: Serializer
    {
        serializer.serialize_str(&date.timestamp_millis().to_string())
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<DateTime<Utc>, D::Error>
        where D: Deserializer<'de>,
    {
        Ok(Utc.timestamp_millis(String::deserialize(deserializer)?.parse().map_err(serde::de::Error::custom)?))
    }
}

use mongodb::{bson::doc, Collection};

use crate::model::{User, Session, Room, Member, Channel, Message, UserId, SessionId, RoomId, ChannelId, MessageId};

#[derive(Clone)]
pub struct Database {
    pub users: Collection<User>,
    pub sessions: Collection<Session>,
    pub rooms: Collection<Room>,
    pub members: Collection<Member>,
    pub channels: Collection<Channel>,
    pub messages: Collection<Message>,
}

impl Database {
    pub fn new(db: mongodb::Database) -> Self {
        Self {
            users: db.collection("users"),
            sessions: db.collection("sessions"),
            rooms: db.collection("room"),
            members: db.collection("members"),
            channels: db.collection("channels"),
            messages: db.collection("messages"),
        }
    }

    pub async fn get_user(&self, user_id: &UserId) -> Option<User> {
        self.users.find_one(doc! { "_id": user_id.0 }, None).await.unwrap()
    }

    pub async fn get_user_by_name(&self, name: &str) -> Option<User> {
        self.users.find_one(doc! { "name": name }, None).await.unwrap()
    }

    pub async fn get_session(&self, session_id: &SessionId) -> Option<Session> {
        self.sessions.find_one(doc! { "_id": session_id.0 }, None).await.unwrap()
    }

    pub async fn get_room(&self, room_id: &RoomId) -> Option<Room> {
        self.rooms.find_one(doc! { "_id": room_id.0 }, None).await.unwrap()
    }

    pub async fn get_member(&self, user_id: &UserId, room_id: &RoomId) -> Option<Member> {
        self.members.find_one(doc! { "userId": user_id.0, "roomId": room_id.0 }, None).await.unwrap()
    }

    pub async fn get_channel(&self, channel_id: &ChannelId) -> Option<Channel> {
        self.channels.find_one(doc! { "_id": channel_id.0 }, None).await.unwrap()
    }

    pub async fn get_message(&self, message_id: &MessageId) -> Option<Message> {
        self.messages.find_one(doc! { "_id": message_id.0 }, None).await.unwrap()
    }
}

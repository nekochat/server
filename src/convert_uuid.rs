use mongodb::bson;

pub trait BsonExt {
    fn into_uuid(self) -> uuid::Uuid;
}

impl BsonExt for bson::Uuid {
    fn into_uuid(self) -> uuid::Uuid {
        uuid::Uuid::from_bytes(self.bytes())
    }
}

pub trait UuidExt {
    fn into_bson(self) -> bson::Uuid;
}

impl UuidExt for uuid::Uuid {
    fn into_bson(self) -> bson::Uuid {
        bson::Uuid::from_bytes(self.into_bytes())
    }
}
